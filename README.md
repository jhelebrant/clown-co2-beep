# `clown-co2-beep`

Gets data from BigClown sensors & plays a sound notification when CO₂ level crosses the limit for given room, prompting the user to open a window (or to go home 🙃).

## Dependencies

- Python >= 3.6
- requests
- aplay from alsa-utils

## Usage

Before the first run:

```
$ virtualenv -p `which python3.6` .venv
$ source .venv/bin/activate
$ pip install -r requirements.txt
```

Running:

```
$ python beep.py <room> <limit>
# for example:
$ python beep.py Plzen-dole 1600
# wrap multi-word names into quotes:
$ python beep.py "Ondrej Filip 5.p." 1500
```

Listing available rooms:

```
$ python rooms.py
"Zdenek Bruna 5.p."                                            
"Asist 5.p."
"Beda 5.p."
"Martin Peterka 5.p."
"Ondrej Filip 5.p."
"Knot"
"Jara Talir 4.p."
"nic-admins 4.p."
"Marketing"
"Tech-dep"
"fred-admins 3.p."
"Labs Bird 2.p."
"Labs HW 2.p."
"PR"
"Plzen-nahore"
"Plzen-dole"
```

Note: `beep.py` creates files in `/tmp` to store the CO₂ level between runs. This prevents annoying repeated beeps when the windows are already open, and concentration started decreasing, but is still above the limit. The files are named `/tmp/co2-<first 10 chars of room name's sha1 hash>`. They're safe to delete (it would just beep the next time, and write a new file).

### Cron job to run the script every few minutes:

```
*/5  *  *  *  1-5  /path/to/clown-co2-beep/.venv/bin/python /path/to/clown-co2-beep/beep.py Plzen-dole 1600
# every 5 min, Monday – Friday
```

### Using other players instead of `aplay`

Change the `player` variable at the top of `beep.py` to your player name. It needs to be able to play a wav file, and to be callable from commandline as `player sound.wav`. Works with `aplay`, `mpv`, `mplayer`, and probably many others.

### Using another sound

Just replace `sound.wav`. You can use any format the player supports (mp3, wav, …). The default sound is `message-new-instant.oga` from the [default freedesktop.org sound theme](https://www.freedesktop.org/wiki/Specifications/sound-theme-spec/).

## Safe indoor CO₂ levels
| CO₂ ppm |     |
| --- | --- |
| 250-350 |	Normal background concentration in outdoor ambient air. |
| 350-1000	| Concentrations typical of occupied indoor spaces with good air exchange. |
| 1000-2000	| Complaints of drowsiness and poor air. |
| 2000-5000 	| Headaches, sleepiness and stagnant, stale, stuffy air. Poor concentration, loss of attention, increased heart rate and slight nausea may also be present. |
| 5000	| Workplace exposure limit (as 8-hour TWA) in most jurisdictions. |
| > 40000 	| Exposure may lead to serious oxygen deprivation resulting in permanent brain damage, coma, even death. |

Source: [Kane International Ltd.](https://www.kane.co.uk/knowledge-centre/what-are-safe-levels-of-co-and-co2-in-rooms)
