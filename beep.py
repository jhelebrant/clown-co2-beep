from subprocess import call
from urllib.parse import quote
import time
import hashlib
import os
import requests
import sys
import urllib3

player = "aplay"

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

if len(sys.argv) != 3 or sys.argv[2].isdigit() is False:
    print(f"Usage: {sys.argv[0]} <room> <limit>,"
          f"eg. {sys.argv[0]} Plzen-dole 2000",
          file=sys.stderr)
    sys.exit(1)

room_name_encoded = quote(sys.argv[1].replace(".", "\."))
time_to = int(time.time() * 1000)
time_from = time_to - 10 * 60 * 1000  # last 10 min
url = (f"https://bigclown.labs.office.nic.cz/api/datasources/proxy/1/query?db="
       f"node&q=SELECT%20%22value%22%20FROM%20%22concentration%22%20WHERE%20%2"
       f"2device_loc%22%20%3D~%20%2F%5E{room_name_encoded}%24%2F%20AND%20time%"
       f"20%3E%20{time_from}ms%20and%20time%20%3C%20{time_to}ms&epoch=ms")
r = requests.get(url, verify=False)
try:
    co2 = int(r.json()["results"][0]["series"][0]["values"][-1][1])
except KeyError:
    print(f"Room “{sys.argv[1]}” does not exist, or has no data available.",
          file=sys.stderr)
    sys.exit(1)
limit = int(sys.argv[2])
roomhash = hashlib.sha1(sys.argv[1].encode('utf-8')).hexdigest()[0:10]
tmpfile = f"/tmp/co2-{roomhash}"

try:
    f = open(tmpfile, "r")
    prev = f.read()
except FileNotFoundError:
    f = open(tmpfile, "w")
    prev = ""
f.close()

if prev.isdigit():
    prev_co2 = int(prev)
else:
    prev_co2 = co2

f = open(tmpfile, "w")
f.write(str(co2))
f.close()

if co2 > limit and co2 >= (prev_co2 + 10):
    devnull = open(os.devnull, "w")
    call([player, os.path.dirname(os.path.realpath(__file__)) + "/sound.wav"],
         stdout=devnull, stderr=devnull)

sys.exit(0)
