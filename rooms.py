import requests
import urllib3
import re
from urllib.parse import unquote

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
r = requests.get("https://bigclown.labs.office.nic.cz/api/dashboards"
                 "/db/rozmisteni-senzoru", verify=False)
data = r.json()["dashboard"]["rows"][0]["panels"][0]["content"]
pattern = r"var-device_loc=([^=?&)]+)"
rooms = re.findall(pattern, data)
for room in rooms:
    print(f"\"{unquote(room)}\"")
